extends Node2D

func _physics_process(delta):
	var gravitySources : Array = get_tree().get_nodes_in_group("GravitySource")
	var gravityBodies : Array = get_tree().get_nodes_in_group("GravityBody")

	for gravityBody in gravityBodies:
		var totalForce : Vector2 = Vector2()
		for gravitySource in gravitySources:
			if not "mass" in gravitySource:
				continue
				
			var distanceVector : Vector2 = gravitySource.global_position - gravityBody.global_position
				
			var force = distanceVector.normalized() * Constants.gravitationalConstant * gravitySource.mass * gravityBody.mass / distanceVector.length_squared()
			totalForce += force

		gravityBody.apply_impulse(Vector2(), totalForce * delta)
		
		if gravityBody is Player and "gravityVector" in gravityBody:
			gravityBody.gravityVector = totalForce
