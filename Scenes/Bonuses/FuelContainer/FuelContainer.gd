extends OrbitalObject
class_name FuelContainer

export (float) var specificImpulseAmount : float = 15.0

func _ready():
	mapIconScene = preload("res://Scenes/Map/ShipIcon/FuelIcon.tscn")
	mapIcon = mapIconScene.instance()

func _on_Area2D_body_entered(body):
	if body is Player:
		if body.has_method("add_fuel"):
			body.add_fuel(specificImpulseAmount)
		queue_free()
