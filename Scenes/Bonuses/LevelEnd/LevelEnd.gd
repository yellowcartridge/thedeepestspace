extends OrbitalObject
class_name LevelEnd

onready var animationPlayer : AnimationPlayer = $AnimationPlayer

func _ready():
	mapIconScene = preload("res://Scenes/Map/ShipIcon/AnomalyIcon.tscn")
	mapIcon = mapIconScene.instance()
	animationPlayer.play("Rotation")
	
func _on_Area2D_body_entered(body):
	if body is Player:
		if body.has_method("target_reached"):
			body.target_reached()
#		queue_free()


func _on_AnimationPlayer_animation_finished(anim_name):
	animationPlayer.play("Rotation")
