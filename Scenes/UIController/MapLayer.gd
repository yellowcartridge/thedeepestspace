extends Control

export (float) var map_scale : float = 0.1

onready var objects : Node2D = $Objects

func _ready():
	update_map_objects()

func _physics_process(delta):
	if not visible:
		return
		
	update_map_objects()

func update_map_objects() -> void:
	var mapObjects : Array = get_tree().get_nodes_in_group("MapObjects")

	var existing_objects : Array = objects.get_children()
	while not existing_objects.empty():
		objects.remove_child(existing_objects.pop_back())
		
	for mapObject in mapObjects:
		if "mapIcon" in mapObject:
			objects.add_child(mapObject.mapIcon)
			mapObject.mapIcon.set_global_position(rect_size / 2 + mapObject.global_position * map_scale)
