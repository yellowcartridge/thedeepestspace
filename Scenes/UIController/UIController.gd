extends CanvasLayer

export (NodePath) var playerPath : NodePath = NodePath()

onready var player : Node = get_node_or_null(playerPath)
onready var fuelAmountBar : ProgressBar = $UICanvas/FuelAmount
onready var mapLayer : Control = $MapLayer
onready var navigationLayer : Control = $NavigationLayer

var map_opened : bool = false
var navigation_enabled : bool = false

func _ready():
	if not player:
		set_process(false)
		return
		
	player.connect("fuel_amount_changed", self, "player_fuel_amount_changed")
	
	player_fuel_amount_changed(player.specificImpulse, player.max_specific_impulse)
	
	map_turn_off()
	navigation_turn_off()

func player_fuel_amount_changed(amount : float, max_amount : float) -> void:
	fuelAmountBar.set_value(amount)
	fuelAmountBar.set_max(max_amount)

func _input(event):
	if Input.is_action_just_pressed("toggle_map"):
		if map_opened:
			map_turn_off()
		else:
			map_turn_on()
			
	if Input.is_action_just_pressed("toggle_navigation"):
		if navigation_enabled:
			navigation_turn_off()
		else:
			navigation_turn_on()

func map_turn_on() -> void:
	map_opened = true
	mapLayer.visible = map_opened
	
func map_turn_off() -> void:
	map_opened = false
	mapLayer.visible = map_opened

func navigation_turn_on() -> void:
	navigation_enabled = true
	navigationLayer.visible = navigation_enabled
	
func navigation_turn_off() -> void:
	navigation_enabled = false
	navigationLayer.visible = navigation_enabled
