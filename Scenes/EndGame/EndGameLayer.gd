extends Control

export (NodePath) var playerPath : NodePath = NodePath()

onready var player : Player = get_node_or_null(playerPath)
onready var popup : Control = $Popup
onready var loseLabel : Label = $Popup/LoseLabel
onready var winLabel : Label = $Popup/WinLabel

func _ready():
	if not player:
		return
		
	player.connect("game_over", self, "game_finished")
	popup.visible = false
	
	set_process(false)
	
func game_finished(isVictory) -> void:
	popup.visible = true
	loseLabel.visible = not isVictory
	winLabel.visible = isVictory
	set_process(true)
	
func _process(delta):
	if Input.is_action_pressed("restart"):
		get_tree().change_scene("res://Scenes/World/World.tscn")
		
	if Input.is_action_pressed("goto_menu"):
		get_tree().change_scene("res://Scenes/TitleScreen/TitleScreen.tscn")
