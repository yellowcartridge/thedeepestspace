extends KinematicBody2D
class_name OrbitalObject

export (String) var objectName : String = ""
export (float) var mass : float = 1000000.0
export (float) var speed : float = 0.002  # rotation speed (in radians)
export (float) var semiMajorAxisLength : float = 5000
export (bool) var destroyPlayer : bool = false

export (PackedScene) var mapIconScene : PackedScene = preload("res://Scenes/Map/ShipIcon/ShipIcon.tscn")
export (NodePath) var parentGravitySourcePath : NodePath = NodePath()

onready var mapIcon : Node2D = mapIconScene.instance()
onready var parentGravitySource : Node = get_node_or_null(parentGravitySourcePath)

var velocity : Vector2 = Vector2()
var orbitRotation : float = 0.0

func _ready():
	if not parentGravitySource or not "mass" in parentGravitySource:
		set_physics_process(false)

func _physics_process(delta):
#	orbitRotation += speed * delta
#	global_position = global_position.rotated(orbitRotation)
#	return
#	semiMajorAxisLength = 5000
	var distanceVector : Vector2 = parentGravitySource.global_position - global_position
#
#	var two_div_dist : float = 2.0 / distanceVector.length()
#
#	if is_nan(two_div_dist) or is_inf(two_div_dist):
#		return
#
#	var orbital_speed : float = sqrt(Constants.gravitationalConstant * parentGravitySource.mass * (two_div_dist - 1.0 / semiMajorAxisLength))
#
	var orbital_speed : float = Constants.gravitationalConstant * parentGravitySource.mass / distanceVector.length_squared()
	var speed_vector : Vector2 = distanceVector.normalized().rotated(PI/2) * orbital_speed
	velocity = speed_vector + parentGravitySource.velocity
	velocity = move_and_slide(velocity)
