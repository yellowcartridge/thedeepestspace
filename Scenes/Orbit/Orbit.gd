extends Node2D

export (float) var speed : float = 2  # rotation speed (in radians)
export (float) var radius : float = 100  # desired orbit radius

func _process(delta):
	rotation += speed * delta
