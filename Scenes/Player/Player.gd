extends RigidBody2D
class_name Player

export (float) var thrust : float = 128
export (float) var max_angular_velocity : float = 2*PI
export (float) var max_specific_impulse : float = 15.0
export (float) var zoom_speed : float = 1.0
export (float) var min_zoom : float = 0.2
export (float) var max_zoom : float = 10.0
export (bool) var infinite_fuel : float = false
export (int) var suicideTime : int  = 5
export (float) var velocityScale : float  = 0.2
export (float) var gravityScale : float  = 0.5

export (PackedScene) var mapIconScene : PackedScene = preload("res://Scenes/Map/ShipIcon/ShipIcon.tscn")

var thrustForce : Vector2 = Vector2()
var gravityVector : Vector2 = Vector2()
var rotation_dir : float = 0
var current_zoom : float = 1.0

onready var specificImpulse : float = max_specific_impulse
onready var suicide_counter : int = suicideTime
onready var mapIcon : Node2D = mapIconScene.instance()
onready var gameEnded : bool = false

onready var flameSprite : AnimatedSprite = $FlameSprite
onready var suicideTimer : Timer = $SuicideTimer
onready var suicideTimeLabel : Label = $Labels/SuicideTimeLabel
onready var labelsLayer : Position2D = $Labels
onready var gravityLine : Line2D = $Labels/GravityLine
onready var velocityLine : Line2D = $Labels/VelocityLine
onready var camera : Camera2D = $Camera

signal fuel_amount_changed
signal game_over

func _ready():
	pass


func _physics_process(delta):
	process_input(delta)
	
	if gameEnded:
		return
		
	apply_impulse(Vector2(), thrustForce.rotated(rotation) * delta)
	velocityLine.points[1] = linear_velocity * velocityScale
	gravityLine.points[1] = gravityVector * gravityScale
	camera.set_zoom(Vector2(current_zoom, current_zoom))

func process_input(delta):
	
	if gameEnded:
		return
	
	if Input.is_action_pressed("thrust") and (infinite_fuel or specificImpulse > 0):
		thrustForce = Vector2(thrust, 0)
		flameSprite.visible = true
		if not infinite_fuel:
			specificImpulse -= delta
			if is_zero_approx(specificImpulse):
				start_suicide_counter()
			emit_signal("fuel_amount_changed", specificImpulse, max_specific_impulse)
	else:
		thrustForce = Vector2()
		flameSprite.visible = false
	
	rotation_dir = 0
	if Input.is_action_pressed("rotate_left"):
		rotation_dir -= 1.0
		set_angular_velocity(-max_angular_velocity)
	elif Input.is_action_pressed("rotate_right"):
		rotation_dir += 1.0
		set_angular_velocity(max_angular_velocity)
	else:
		set_angular_velocity(0)
		
	rotation_dir = clamp(rotation_dir, -1.0, 1.0)
	labelsLayer.set_rotation(-rotation)
	mapIcon.set_rotation(rotation)
	
	if Input.is_action_pressed("zoom_in"):
		current_zoom -= zoom_speed * delta
	elif Input.is_action_pressed("zoom_out"):
		current_zoom += zoom_speed * delta
		
	current_zoom = clamp(current_zoom, min_zoom, max_zoom)


func add_fuel(amount : float):
	specificImpulse += amount
	specificImpulse = clamp(specificImpulse, 0.0, max_specific_impulse)
	stop_suicide_counter()
	emit_signal("fuel_amount_changed", specificImpulse, max_specific_impulse)


func target_reached() -> void:
	if gameEnded:
		return
	gameEnded = true
	emit_signal("game_over", true)


func die():
	if not gameEnded:
		gameEnded = true
		emit_signal("game_over", false)
	queue_free()


func _on_Player_body_entered(body):
	if "destroyPlayer" in body and body.destroyPlayer:
		die()

func start_suicide_counter() -> void:
	if not suicideTimer.is_stopped():
		return

	suicideTimer.start()
	suicideTimeLabel.visible = true
	suicideTimeLabel.set_text(String(suicide_counter))
	
func stop_suicide_counter() -> void:
	suicide_counter = suicideTime
	suicideTimer.stop()
	suicideTimeLabel.visible = false

func _on_SuicideTimer_timeout():
	suicide_counter -= 1
	suicideTimeLabel.set_text(String(suicide_counter))
	
	if suicide_counter <= 0:
		stop_suicide_counter()
		die()
