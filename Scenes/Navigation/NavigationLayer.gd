extends Control

export (PackedScene) var gravityPointerScene : PackedScene = preload("res://Scenes/Navigation/Pointer/GravityPointer.tscn")
export (PackedScene) var fuelPointerScene : PackedScene = preload("res://Scenes/Navigation/Pointer/FuelPointer.tscn")
export (PackedScene) var anomalyPointerScene : PackedScene = preload("res://Scenes/Navigation/Pointer/AnomalyPointer.tscn")
export (NodePath) var playerPath : NodePath = NodePath()

onready var pointers : Node2D = $Pointers
onready var player : Player = get_node_or_null(playerPath)

func _ready():
	if not player:
		return
		
	add_navigation_pointers(get_tree().get_nodes_in_group("GravitySource"), gravityPointerScene)
	add_navigation_pointers(get_tree().get_nodes_in_group("FuelContainers"), fuelPointerScene)
	add_navigation_pointers(get_tree().get_nodes_in_group("Anomalies"), anomalyPointerScene)

func add_navigation_pointers(objects : Array, pointerScene : PackedScene) -> void:
	for object in objects:
		var pointer : = pointerScene.instance()
		pointer.Init(object, player, self)
		pointers.add_child(pointer)
