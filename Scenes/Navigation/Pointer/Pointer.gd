extends Node2D

onready var label : Label = $Label

var objectToPointId : int = -1
var objectPointFromId : int = -1

var canvas : Control = null

func Init(to : Node2D, from : Node2D, control : Control):
	objectToPointId = to.get_instance_id()
	objectPointFromId = from.get_instance_id()
	canvas = control

func _ready():
	var objectToPoint : = instance_from_id(objectToPointId)
	var objectPointFrom : = instance_from_id(objectPointFromId)
	
	if not objectToPoint or not objectPointFrom or not canvas:
		set_process(false)
		
	label.set_text(objectToPoint.objectName)
		
func _process(delta):
	var objectToPoint : = instance_from_id(objectToPointId)
	var objectPointFrom : = instance_from_id(objectPointFromId)
	
	if not objectToPoint:
		queue_free()
		return
		
	if not objectPointFrom or not canvas:
		return
	
	var zoom : float = objectPointFrom.current_zoom if "current_zoom" in objectPointFrom else 1.0
	
	var screen_size : Vector2 = canvas.get_size()
	var toTarget : Vector2 = objectToPoint.global_position - objectPointFrom.global_position
	toTarget /= zoom
	toTarget.x = clamp(toTarget.x, -screen_size.x / 2, screen_size.x / 2)
	toTarget.y = clamp(toTarget.y, -screen_size.y / 2, screen_size.y / 2)
	
	set_global_position(toTarget + screen_size / 2)
	set_rotation(toTarget.angle())
