shader_type canvas_item;
render_mode unshaded;

uniform float time;
uniform float lines_count;
uniform float wave_amplitude;

void fragment()
{
	float offset = sin(time+UV.y*lines_count)*wave_amplitude;
	COLOR = texture(TEXTURE, vec2(UV.x + offset, UV.y));
}